enum TraingleType{
    ISOSCELES,
    EQUILATERAL,
    SCALENE
}

public class Triangle
{
    private int a, b, c;
    private TraingleType type;

    public Triangle(int a, int b, int c) throws TriangleException
    {
        if((a <= 0) || (b <= 0) || (c <= 0))
        {
            throw new TriangleException("Incorrect data - values less than or equal to 0");
        }
        else if((a >= b+c) || (b >= a+c) || (c >= a+b))
        {
            throw new TriangleException("Incorrect data");
        }
        else
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }

    public TraingleType getType()
    {
        if((a == b) && (b == c))
        {
            this.type = TraingleType.EQUILATERAL;
        }
        else if((a == b) || (a == c) || (b==c))
        {
            this.type = TraingleType.ISOSCELES;
        }
        else
        {
            this.type = TraingleType.SCALENE;
        }

        return this.type;
    }
}
