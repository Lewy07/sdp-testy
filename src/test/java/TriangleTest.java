import org.junit.jupiter.api.Assertions;

class TriangleTest {

    @org.junit.jupiter.api.Test
    void wrongTriangle() {
        Assertions.assertThrows(TriangleException.class , () -> {
            Triangle t1 = new Triangle(0,0,1);
        });

        Assertions.assertThrows(TriangleException.class , () -> {
            Triangle t1 = new Triangle(10,10,20);
        });
    }

    @org.junit.jupiter.api.Test
    void getType() throws  TriangleException {
        Triangle t1 = new Triangle(10, 10,10);
        Triangle t2 = new Triangle(3, 4,5);
        Triangle t3 = new Triangle(5, 5,8);

        Assertions.assertEquals(t1.getType(), TraingleType.EQUILATERAL);
        Assertions.assertEquals(t2.getType(), TraingleType.SCALENE);
        Assertions.assertEquals(t3.getType(), TraingleType.ISOSCELES);
    }
}